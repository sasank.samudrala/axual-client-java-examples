<?xml version="1.0" encoding="UTF-8"?>
<!--
    Licensed to the Apache Software Foundation (ASF) under one
    or more contributor license agreements.  See the NOTICE file
    distributed with this work for additional information
    regarding copyright ownership.  The ASF licenses this file
    to you under the Apache License, Version 2.0 (the
    "License"); you may not use this file except in compliance
    with the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on an
    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, either express or implied.  See the License for the
    specific language governing permissions and limitations
    under the License.
    -->

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>io.axual.client.example</groupId>
    <artifactId>axual-client-proxy-avro-producer</artifactId>
    <version>6.0.1</version>

    <organization>
        <name>Axual B.V.</name>
        <url>http://www.axual.com</url>
    </organization>

    <licenses>
        <license>
            <name>Apache 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <properties>
        <apache.avro.version>1.10.2</apache.avro.version>
        <axual.client.version>6.0.1</axual.client.version>
        <slf4j.version>1.7.32</slf4j.version>
        <jackson.version>2.12.2</jackson.version>
        <java.source.version>1.8</java.source.version>
        <java.target.version>1.8</java.target.version>
        <maven.compiler.plugin.version>3.8.0</maven.compiler.plugin.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>io.axual.client</groupId>
            <artifactId>axual-client-proxy</artifactId>
            <version>${axual.client.version}</version>
        </dependency>
        <!-- Logging -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j.version}</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>${slf4j.version}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-core</artifactId>
            <version>${jackson.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.avro</groupId>
            <artifactId>avro</artifactId>
            <version>${apache.avro.version}</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.avro</groupId>
                <artifactId>avro-maven-plugin</artifactId>
                <version>${apache.avro.version}</version>
                <executions>
                    <execution>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>schema</goal>
                        </goals>
                        <configuration>
                            <sourceDirectory>${project.basedir}/src/main/resources/schemas</sourceDirectory>
                            <outputDirectory>${project.build.directory}/generated-sources/avro
                            </outputDirectory>
                            <imports>
                                <import>${project.basedir}/src/main/resources/schemas/Application.avsc</import>
                                <import>${project.basedir}/src/main/resources/schemas/ApplicationLogLevel.avsc
                                </import>
                                <import>${project.basedir}/src/main/resources/schemas/ApplicationLogEvent.avsc
                                </import>
                                <import>
                                    ${project.basedir}/src/main/resources/schemas/ApplicationLogAlertSetting.avsc
                                </import>
                            </imports>
                            <stringType>CharSequence</stringType>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven.compiler.plugin.version}</version>
                <configuration>
                    <source>${java.source.version}</source>
                    <target>${java.target.version}</target>
                    <compilerArgs>
                        <arg>-Xlint:none</arg>
                    </compilerArgs>
                </configuration>
            </plugin>

        </plugins>
    </build>
</project>